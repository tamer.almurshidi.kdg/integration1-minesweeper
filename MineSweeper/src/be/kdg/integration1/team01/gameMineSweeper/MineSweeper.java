package be.kdg.integration1.team01.gameMineSweeper;

import be.kdg.integration1.team01.gameMineSweeper.model.Board;

import java.util.Scanner;

public class MineSweeper {

    static Board board;
    static Scanner keyboard;
    public static void main(String[] args) {

        keyboard = new Scanner(System.in);
        board = new Board();
        board.start();
    }
}
