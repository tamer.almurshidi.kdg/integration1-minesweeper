package be.kdg.integration1.team01.gameMineSweeper.model;

import java.util.Scanner;

public class Board {
    private final Scanner keyboard;
    private final MineSweeperGridBoard mineSweeperGridBoard;
    public static final int GRID_SIZE = 9;
    public static final int MINE_COUNT = 10;

    public Board() {
        keyboard = new Scanner(System.in);
        mineSweeperGridBoard = new MineSweeperGridBoard(GRID_SIZE, MINE_COUNT);
    }

    public void start() {
        while (!mineSweeperGridBoard.isWon()){
            switchScreen(ScreenName.MainScreen);
        }
    }

    public void playGame() {

    }

    public void switchScreen(ScreenName screenName) {
        ScreenName tempScreenName;
        switch (screenName) {
            case MainScreen -> {
                 DisplayView.displayMainScreen();
                int input = keyboard.nextInt();
                tempScreenName = convertMainScreenNumberToName(input);
                switchScreen(tempScreenName);
            }
            case HelpScreen -> {
                DisplayView.displayHelpScreen();
                getUserInputAndSwitchScreen();
            }
            case RulesScreen -> {
                DisplayView.displayRulesScreen();
                getUserInputAndSwitchScreen();
            }
            case MineSweeperBoard -> {
                DisplayView.displayMineSweeperBoard();
            }
            case LeaderboardScreen -> {
                DisplayView.displayLeaderBoard();
                getUserInputAndSwitchScreen();
            }
            case Exit -> System.exit(0);
            default -> DisplayView.displayInvalidCommand();
        }
    }

    private void getUserInputAndSwitchScreen() {
        int input = keyboard.nextInt();
        ScreenName screenName = convertScreenNumberToName(input);
        switchScreen(screenName);
    }

    private ScreenName convertMainScreenNumberToName(int userInput) {
        return switch (userInput) {
            case 1 -> ScreenName.RulesScreen;
            case 2 -> ScreenName.HelpScreen;
            case 3 -> ScreenName.LeaderboardScreen;
            case 4 -> ScreenName.MineSweeperBoard;
            case 0 -> ScreenName.Exit;
            default -> ScreenName.MainScreen;
        };
    }

    private ScreenName convertScreenNumberToName(int userInput) {
        return switch (userInput) {
            case 0 -> ScreenName.Exit;
            default -> ScreenName.MainScreen;
        };
    }

    public boolean isInputOptionValid(int userInput, ScreenName screenName) {
        return true;
    }
}
