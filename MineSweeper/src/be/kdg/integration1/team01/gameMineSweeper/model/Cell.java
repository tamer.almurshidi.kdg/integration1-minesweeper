package be.kdg.integration1.team01.gameMineSweeper.model;

public class Cell {
    private boolean isRevealed;
    private boolean isMine;
    private boolean isFlagged;
    private int neighbours;

    public void reveal() {
        isRevealed = true;
    }

    public void setMineCell() {
        isMine = true;
    }

    public void addNeighbour() {
        neighbours++;
    }

    public boolean getIsMine() {
        return isMine;
    }

    public void toggleIsFlagged() {
        isFlagged = !isFlagged;
    }

    public boolean getIsFlagged() {
        return isFlagged;
    }

    public void setIsFlagged(boolean flagged) {
        isFlagged = flagged;
    }

    public int getNeighbours() {
        return neighbours;
    }

    @Override
    public String toString() {
        return "?";
    }
}
