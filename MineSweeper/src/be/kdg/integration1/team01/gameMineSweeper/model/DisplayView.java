package be.kdg.integration1.team01.gameMineSweeper.model;

public final class DisplayView {
    public static void displayLeaderBoard() {
        displayScreenTitle(TextConstant.LEADERBOARD_SCREEN_TITLE);

        System.out.println("10/11/2023 Score: 10 ");
        System.out.println("10/11/2023 Score: 10 ");
        System.out.println("10/11/2023 Score: 10 ");

        System.out.println(TextConstant.MAIN_PAGE_OPTION);
        System.out.println(TextConstant.EXIT_OPTION);

        displayAskUserEnterOption();
    }

    public static void displayMainScreen() {
        displayScreenTitle(TextConstant.MAIN_SCREEN_TITLE);

        System.out.println("Team 1 is developing mine sweeper ASCII game.\n" +
                "A minesweeper is a small warship designed to remove \n" +
                "or detonate naval mines. Using various mechanisms \n" +
                "intended to counter the threat posed by naval mines, \n" +
                "minesweepers keep waterways clear for safe shipping.");

        System.out.println(TextConstant.RULES_PAGE_OPTION);
        System.out.println(TextConstant.HELP_PAGE_OPTION);
        System.out.println(TextConstant.LEADERBOARD_PAGE_OPTION);
        System.out.println(TextConstant.GAME_BOARD_PAGE_OPTION);
        System.out.println(TextConstant.EXIT_OPTION);

        displayAskUserEnterOption();
    }

    public static void displayHelpScreen() {
        displayScreenTitle(TextConstant.HELP_SCREEN_TITLE);

        System.out.println("1. Start by entering column number and row number on a square.\n" +
                "2. If the square is safe, a number will be revealed, indicating how many mines \n" +
                "are adjacent to it.\n" +
                "3. If the square contains a mine, the game is over and you lose.\n" +
                "4. You can use the numbers to deduce where other mines are located.\n" +
                "5. If you are unsure about whether a square is safe or contains a mine, you \n" +
                "can enter \"y\" after choosing row and column to flag it as a potential mine.\n" +
                "Otherwise press enter.\n" +
                "6. Once you have flagged all of the potential mines, you can start opening \n" +
                "the remaining squares.\n" +
                "7. If you open all of the safe squares and avoid detonating any mines, you \n" +
                "win the game.");

        System.out.println(TextConstant.MAIN_PAGE_OPTION);
        System.out.println(TextConstant.EXIT_OPTION);

        displayAskUserEnterOption();
    }

    public static void displayRulesScreen() {
        displayScreenTitle(TextConstant.RULES_SCREEN_TITLE);

        System.out.println("1. Start by entering column number and row number on a square.\n" +
                "2. If the square is safe, a number will be revealed, indicating how many mines \n" +
                "are adjacent to it.\n" +
                "3. If the square contains a mine, the game is over and you lose.\n" +
                "4. You can use the numbers to deduce where other mines are located.\n" +
                "5. If you are unsure about whether a square is safe or contains a mine, you \n" +
                "can enter \"y\" after choosing row and column to flag it as a potential mine.\n" +
                "Otherwise press enter.\n" +
                "6. Once you have flagged all of the potential mines, you can start opening \n" +
                "the remaining squares.\n" +
                "7. If you open all of the safe squares and avoid detonating any mines, you \n" +
                "win the game.");

        System.out.println(TextConstant.MAIN_PAGE_OPTION);
        System.out.println(TextConstant.EXIT_OPTION);

        displayAskUserEnterOption();
    }

    public static void displayMineSweeperBoard() {
        displayScreenTitle(TextConstant.MINESWEEPER_GAME_BOARD_SCREEN_TITLE);
    }

    public static void displayScreenTitle(String title) {
        System.out.println(title);
    }

    public static void displayAskUserEnterOption() {
        System.out.println(TextConstant.ENTER_NUMBER_OF_OPTION);
    }

    public static void displayBanner(String banner) {
        System.out.println(banner);
    }

    public static void displayInvalidCommand() {
        System.out.println(TextConstant.INVALID_COMMAND);
    }

    public static void displayMineSweeperBoard(Cell[][] cells) {
        System.out.println("1 2 3 4 5 6 7 8 9");
        System.out.println("? ? ? ? ? ? ? ? ?");
    }

    public static void displayAskUserRowNumber() {
        System.out.println(TextConstant.ENTER_ROW_NUMBER);
    }

    public static void displayAskUserColumnNumber() {
        System.out.println(TextConstant.ENTER_COLUMN_NUMBER);
    }

    public static void displayMineAndRevealCount(int mineCount, int revealCount, int gridSize) {
        System.out.printf(TextConstant.MINE_AND_REVEAL_COUNT, mineCount, revealCount, gridSize);
    }
}
