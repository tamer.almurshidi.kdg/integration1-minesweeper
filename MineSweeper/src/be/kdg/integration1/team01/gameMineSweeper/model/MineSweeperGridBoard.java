package be.kdg.integration1.team01.gameMineSweeper.model;

public class MineSweeperGridBoard {
    private int gridSize;
    private int mineCount;
    private int revealedCellCount;
    private Cell[][] cells;
    private Position position;

    public MineSweeperGridBoard(int gridSize, int mineCount) {
        this.gridSize = gridSize;
        this.mineCount = mineCount;
    }

    public void initMineSweeperGridBoard() {

    }

    public boolean isInputPositionValid(Position position) {
        return true;
    }

    public boolean isCellRevealed(Position position) {
        return true;
    }

    public boolean isCellMine(Position position) {
        return true;
    }

    public boolean isCellFlagged(Position position) {
        return true;
    }

    public void changeFlagCell(Position position) {

    }

    public void addMines(Cell[][] cells) {

    }

    public boolean isWon() {
        return false;
    }

    public void revealAllNeighbours() {

    }
}
