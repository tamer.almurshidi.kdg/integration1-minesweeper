package be.kdg.integration1.team01.gameMineSweeper.model;

public class Position {
    private int row;
    private int column;

    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }
}
