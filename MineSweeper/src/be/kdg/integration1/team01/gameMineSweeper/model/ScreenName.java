package be.kdg.integration1.team01.gameMineSweeper.model;

public enum ScreenName {
    MainScreen,
    HelpScreen,
    MineSweeperBoard,
    LeaderboardScreen,
    RulesScreen,
    Exit;
}
