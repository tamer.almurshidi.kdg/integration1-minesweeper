package be.kdg.integration1.team01.gameMineSweeper.model;

public final class TextConstant {
    public static final String EXIT_OPTION = "0. EXIT";
    public static final String MAIN_PAGE_OPTION = "1. Main Page";
    public static final String RULES_PAGE_OPTION = "1. Rules";
    public static final String HELP_PAGE_OPTION = "2. Help";
    public static final String LEADERBOARD_PAGE_OPTION = "3. Leaderboard";
    public static final String GAME_BOARD_PAGE_OPTION = "4. Game board (start)";
    public static final String ENTER_NUMBER_OF_OPTION = "Please enter the number of the option: ";
    public static final String ENTER_ROW_NUMBER = "Enter the row number: ";
    public static final String ENTER_COLUMN_NUMBER = "Enter the column number: ";
    public static final String DO_YOU_WANT_FLAG = "Do you want to flag it (n default/y)? ";
    public static final String MAIN_SCREEN_TITLE = "Welcome";
    public static final String LEADERBOARD_SCREEN_TITLE = "Leaderboard";
    public static final String RULES_SCREEN_TITLE = "RULES";
    public static final String HELP_SCREEN_TITLE = "Help";
    public static final String MINESWEEPER_GAME_BOARD_SCREEN_TITLE = "Mine sweeper game";
    public static final String INVALID_COMMAND = "Invalid command! please enter a valid option.";
    public static final String MINE_AND_REVEAL_COUNT = "Mine: %s - Revealed Cells: %s of %s \n";
}
